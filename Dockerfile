# mosquitto
# para otimizar, fazer outro container que irá gerar as chaves usuarios e arquivos de configuração
# Tentar usar o alpine
# o "CN" Tem quer ser o ip ou dominio registrado do servidor
FROM debian:buster-slim

MAINTAINER Wemerson Bruno Henriques <wbhenriques@gmail.com>

ENV     DEBIAN_FRONTEND=noninteractive \
        MQTT_PORT=1883 \
        MQTT_PORT_SSL=8883 \
        MQTT_PORT_SQT=9001 \
        MQTT_PORT_SQT_SSL=9883
ARG     MQTT_USER=admin \
        MQTT_PASSWD=admin \
        CRT_PAIS=BR \
        CRT_ESTADO=ES \
        CRT_CIDADE=Iuna \
        CRT_EMPRESA=dimloo \
        CRT_SETOR=dimloo \
        CRT_CN=dimloo.com.br \
        CRT_MAIL=wbhenriques@gmail.com
RUN     apt-get update -y && \
        apt-get upgrade -y && \
        apt-get install -y --no-install-recommends \
        --no-install-suggests \
        mosquitto \
        openssl ca-certificates && \
# Gera certificado
        mkdir -p /root/certs && cd /root/certs && \
        openssl genrsa -out ca.key 2048 && \
        openssl req -new -x509 -days 1826 -key ca.key -out ca.crt -subj "/C=$CRT_PAIS/ST=$CRT_ESTADO/L=$CRT_CIDADE/$CRT_EMPRESA/$CRT_SETOR/$CRT_CN/emailAddress=$CRT_MAIL/" && \
        openssl genrsa -out server.key 2048 && \
        openssl req -new -out server.csr -key server.key -subj "/C=$CRT_PAIS/ST=$CRT_ESTADO/L=$CRT_CIDADE/$CRT_EMPRESA/$CRT_SETOR/$CRT_CN/emailAddress=$CRT_MAIL/" && \
        openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 3600 && \
        cp server.key server.crt /etc/mosquitto/certs/ && cp ca.crt /etc/mosquitto/ca_certificates/ && chown root:mosquitto /root/certs > /dev/nul 2>&1 && \
# Limpa instalações
        apt-get clean && \
        apt-get autoremove --purge -y && \
        rm -rf /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
        /usr/share/man/??_* \
        /usr/share/man/?? && \
# Criar usuario:
        touch /etc/mosquitto/passwd && mosquitto_passwd -b /etc/mosquitto/passwd $MQTT_USER $MQTT_PASSWD && \
# Configurações:
        echo "allow_anonymous false" >> /etc/mosquitto/conf.d/default.conf && \
        echo "password_file /etc/mosquitto/passwd" >> /etc/mosquitto/conf.d/default.conf && \
        echo "listener $MQTT_PORT" >> /etc/mosquitto/conf.d/default.conf && \
        echo "listener $MQTT_PORT_SSL" >> /etc/mosquitto/conf.d/default.conf && \
        echo "cafile /etc/mosquitto/ca_certificates/ca.crt">> /etc/mosquitto/conf.d/default.conf && \
        echo "keyfile /etc/mosquitto/certs/server.key" >> /etc/mosquitto/conf.d/default.conf && \
        echo "certfile /etc/mosquitto/certs/server.crt" >> /etc/mosquitto/conf.d/default.conf && \
        echo "require_certificate false" >> /etc/mosquitto/conf.d/default.conf && \
        echo "tls_version tlsv1.2" >> /etc/mosquitto/conf.d/default.conf && \
        echo "listener $MQTT_PORT_SQT" >> /etc/mosquitto/conf.d/default.conf && \
        echo "protocol websockets" >> /etc/mosquitto/conf.d/default.conf && \
        echo "listener $MQTT_PORT_SQT_SSL" >> /etc/mosquitto/conf.d/default.conf && \
        echo "protocol websockets" >> /etc/mosquitto/conf.d/default.conf && \
        echo "cafile /etc/mosquitto/ca_certificates/ca.crt" >> /etc/mosquitto/conf.d/default.conf && \
        echo "keyfile /etc/mosquitto/certs/server.key" >> /etc/mosquitto/conf.d/default.conf && \
        echo "certfile /etc/mosquitto/certs/server.crt" >> /etc/mosquitto/conf.d/default.conf && \
        echo "require_certificate true" >> /etc/mosquitto/conf.d/default.conf && \
        echo "tls_version tlsv1.2" >> /etc/mosquitto/conf.d/default.conf && \
# Acertando permições:
        chown -R mosquitto:mosquitto /etc/mosquitto/

#RUN printenv

#VOLUME ["/etc/mosquitto", "/var/log/mosquitto"]
EXPOSE $MQTT_PORT $MQTT_PORT_SSL $MQTT_PORT_SQT $MQTT_PORT_SQT_SSL
#ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/sbin/mosquitto", "-c", "/etc/mosquitto/mosquitto.conf", "-v"]
