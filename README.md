1) Adicionar um usuário e senha para o MQTT:
 1.1  Com o container montado entre no bash:

        # docker exec -it nome_do_container /bin/bash

 1.2  Comando que adiciona o usuário:

        # mosquitto_passwd -c /etc/mosquitto/passwd usuario
        irá pedir para inserir uma senha e depois para confirmar.

 1.3  Remover usuário:
        # mosquitto_passwd -D /etc/mosquitto/passwd usuario

2) Gera certificado:
 2.1  Para gerar um novo certificado e usuário, edite as variáveis e rode o comando que irá compilar ima nova imagem:

        # docker image build . -t imagem:tag  

 2.5  Testes usando TLS:
        Publicando:
                # mosquitto_pub -t topic/home -m "Publiquei!" -h dimloo.com.br -p 8883 --cafile /etc/mosquitto/certs/server.crt --tls-version tlsv1.2 -u usermqtt -P admin1552

        De outro local da rede:
                # scp -P222 /etc/mosquitto/certs/server.crt www@10.5.0.111:/home/www    (copia a chave do servidor para o cliente remoto)
                # mosquitto_pub -t topic/home -m "Publiquei!" -h dimloo.com.br -p 8883 --cafile /home/www/server.crt --tls-version tlsv1.2 -u usermqtt -P admin1552

3) Instalando mqttbox:
        Instalando biblioteca:

                # apt-get install libgconf-2-4
        Baixar no site:
                # wget -c https://s3-us-west-2.amazonaws.com/workswithweb/mqttbox/latest/linux/MQTTBox.deb
                
        Instalar:
                # dpkg -i MQTTBox.deb   ou      apt install ./ MQTTBox.deb
